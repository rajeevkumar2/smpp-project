package com.life9.smpp;

import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.life9.smpp.model.FileProperties;

@CrossOrigin
@RestController
public class Configure {
	private static final Logger logger = LoggerFactory.getLogger(Configure.class);

	@PostMapping("/config")
	public void writePropertiesToPropertiesFile(@RequestBody FileProperties fileProperties) {
		logger.info("Configure.writePropertiesToPropertiesFile(): {}");
		try {
			Properties property = new Properties();
			property.setProperty("username", fileProperties.getUsername());
			property.setProperty("password", fileProperties.getPassword());

			property.store(new FileWriter("src/main/resources/config.properties"), "Properties file");
			logger.info("Successfully wrote to the config.properties.");
		} catch (IOException e) {
			logger.info("exception stack trace", e);
		}
	}

	@PostMapping("/sendomacp")
	public void displayProperties(@RequestParam("to") String to, @RequestBody FileProperties fileProperties) {
		logger.info("Configure.displayProperties(): {}");
		try {
			FileReader fileReader = new FileReader("src/main/resources/config.properties");
			Properties properties = new Properties();
			properties.load(fileReader);
			logger.info(properties.getProperty("username"));
			logger.info(properties.getProperty("password"));
		} catch (Exception e) {
			logger.info("exception stack trace", e);
		}
	}

	@GetMapping("/get")
	public String display() {
		logger.info("Configure display()");
		System.out.println("Configure display()");
		return "Springboot project successfully dockerize";
	}
}
